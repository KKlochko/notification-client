###############################################################################
# Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>             #
#                                                                             #
# This file is part of notification-client.                                   #
#                                                                             #
# notification-client is free software: you can redistribute it and/or modify #
# it under the terms of the GNU Affero General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or (at your  #
# option) any later version.                                                  #
#                                                                             #
# notification-client is distributed in the hope that it will be useful, but  #
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY  #
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public      #
# License for more details.                                                   #
#                                                                             #
# You should have received a copy of the GNU Affero General Public License    #
# along with notification-client. If not, see <https://www.gnu.org/licenses/>.#
###############################################################################

import asyncio

from notification_client.cli import cli_app


def main():
    cli_app()


if __name__ == '__main__':
    main()

