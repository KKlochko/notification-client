###############################################################################
# Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>             #
#                                                                             #
# This file is part of notification-client.                                   #
#                                                                             #
# notification-client is free software: you can redistribute it and/or modify #
# it under the terms of the GNU Affero General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or (at your  #
# option) any later version.                                                  #
#                                                                             #
# notification-client is distributed in the hope that it will be useful, but  #
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY  #
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public      #
# License for more details.                                                   #
#                                                                             #
# You should have received a copy of the GNU Affero General Public License    #
# along with notification-client. If not, see <https://www.gnu.org/licenses/>.#
###############################################################################

import asyncio
import sys
import typer
from notification_client.MessagePrinter import MessagePrinter

from notification_client.client import Client

cli_app = typer.Typer(rich_markup_mode="rich")
notify = typer.Typer()


@cli_app.command()
def up(
    ip: str = typer.Option(
        "127.0.0.1", "--ip", "-i",
        help="The server's ip.",
        rich_help_panel="Connection",
    ),
    port: int = typer.Option(
        5555, "--port", "-p",
        help="The server's port to notify [b]clients[/].",
        rich_help_panel="Connection",
    ),
    notification_dir: str = typer.Option(
        None, "--notification-dir", "-d",
        help="The client will save notifications to the directory by the path. Does not save if this option is not provided.",
        rich_help_panel="Persistence",
        show_default=False,
    )
):
    """
    This command [b]start[/] the [yellow b]client[/] and connects to the notification server.
    """

    try:
        client = Client(ip, port, notification_dir)
        asyncio.run(client.connect())
    except KeyboardInterrupt:
        MessagePrinter.bye()
        sys.exit(0)
