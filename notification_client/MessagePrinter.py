from rich.console import Console
from rich.markdown import Markdown

class MessagePrinter:
    @staticmethod
    def connected(ip, port, timestamp):
        console = Console()

        console.print(f'[yellow b][INFO][/][#8abeb7 b][{timestamp}][/] Connected to the server ({ip}:{port}).')

    @staticmethod
    def print(title: str, message: str):
        console = Console()

        full_message = f'# {title}\n{message}'

        console.print(Markdown(full_message))

    @staticmethod
    def printInfo(message: str, timestamp: str):
        console = Console()

        console.print(f'[yellow b][INFO][/][#8abeb7 b][{timestamp}][/] {message}')

    @staticmethod
    def bye():
        console = Console()

        console.print('[green]bye[/]')

