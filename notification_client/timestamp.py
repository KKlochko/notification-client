from datetime import datetime

class Timestamp:
    @staticmethod
    def now() -> str:
        return str(datetime.now())

