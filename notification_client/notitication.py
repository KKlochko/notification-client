from notifypy import Notify


class Notification:
    @staticmethod
    def truncatechars(message: str, size: int):
        if len(message) > size:
            return message[:(size-3)] + '...'

        return message

    @staticmethod
    def notify(title: str, message: str):
        notification = Notify()
        notification.title = Notification.truncatechars(title, 80)
        notification.message = Notification.truncatechars(message, 100)
        notification.send()

