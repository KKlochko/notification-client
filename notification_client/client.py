###############################################################################
# Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>             #
#                                                                             #
# This file is part of notification-client.                                   #
#                                                                             #
# notification-client is free software: you can redistribute it and/or modify #
# it under the terms of the GNU Affero General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or (at your  #
# option) any later version.                                                  #
#                                                                             #
# notification-client is distributed in the hope that it will be useful, but  #
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY  #
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public      #
# License for more details.                                                   #
#                                                                             #
# You should have received a copy of the GNU Affero General Public License    #
# along with notification-client. If not, see <https://www.gnu.org/licenses/>.#
###############################################################################

import asyncio
import struct
import sys

from notification_client.MessagePrinter import MessagePrinter
from notification_client.notitication import Notification
from notification_client.timestamp import Timestamp
from notification_client.notification_saver import NotificationSaver


class Client:
    BUFFER_SIZE = 1024
    NOTIFICATION_DIR = None

    def __init__(self, ip: str, port: int, notification_dir: str | None = None):
        self.__ip = ip
        self.__port = port
        self.NOTIFICATION_DIR = notification_dir

    async def connect(self):
        try:
            reader, writer = await asyncio.open_connection(self.__ip, self.__port)
            MessagePrinter.connected(self.__ip, self.__port, Timestamp.now())
        except ConnectionRefusedError:
            now = Timestamp.now()
            sys.stderr.write(f"[ERROR][{now}] Could not connect to the server. Please, check the ip and the port.\n")
            sys.exit(1)

        await self.handle(reader, writer)

    @staticmethod
    async def receive_message(reader) -> str:
        size, = struct.unpack('<L', await reader.readexactly(4))
        message = await reader.readexactly(size)
        return message.decode('utf-8')

    async def handle(self, reader, writer):
        try:
            while True:
                title = await self.receive_message(reader)
                message = await self.receive_message(reader)

                now = Timestamp.now()
                MessagePrinter.printInfo('A notification received from the server.', now)
                MessagePrinter.print(title, message)

                Notification.notify(title, message)

                if not self.NOTIFICATION_DIR is None:
                    NotificationSaver.save_notification(self.NOTIFICATION_DIR, title, message, now)

        except asyncio.CancelledError:
            print('Something went wrong')
        finally:
            writer.close()
            await writer.wait_closed()

